import { ChatMessage } from '.'

let chatMessage

beforeEach(async () => {
  chatMessage = await ChatMessage.create({ message: 'test', isRead: 'test', roomId: 'test', from: 'test', isBot: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = chatMessage.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(chatMessage.id)
    expect(view.message).toBe(chatMessage.message)
    expect(view.isRead).toBe(chatMessage.isRead)
    expect(view.roomId).toBe(chatMessage.roomId)
    expect(view.from).toBe(chatMessage.from)
    expect(view.isBot).toBe(chatMessage.isBot)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = chatMessage.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(chatMessage.id)
    expect(view.message).toBe(chatMessage.message)
    expect(view.isRead).toBe(chatMessage.isRead)
    expect(view.roomId).toBe(chatMessage.roomId)
    expect(view.from).toBe(chatMessage.from)
    expect(view.isBot).toBe(chatMessage.isBot)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
