import mongoose, { Schema } from 'mongoose'

const chatMessageSchema = new Schema({
  message: {
    type: String
  },
  isRead: {
    type: Boolean
  },
  roomId: {
    type: String
  },
  from: {
    type: String
  },
  isBot: {
    type: Boolean
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

chatMessageSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      message: this.message,
      isRead: this.isRead,
      roomId: this.roomId,
      from: this.from,
      isBot: this.isBot,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('chatmessage', chatMessageSchema)

export const schema = model.schema
export default model
