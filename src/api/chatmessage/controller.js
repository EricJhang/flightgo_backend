import { success, notFound } from '../../services/response/'
import { ChatMessage } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  ChatMessage.create(body)
    .then((chatMessage) => chatMessage.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  ChatMessage.find(query, select, cursor)
    .then((chatMessages) => chatMessages.map((chatMessage) => chatMessage.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  ChatMessage.findById(params.id)
    .then(notFound(res))
    .then((chatMessage) => chatMessage ? chatMessage.view() : null)
    .then(success(res))
    .catch(next)

export const showMessageByRoomId = ({ params }, res, next) =>
  ChatMessage.findById({ roomId: params.roomId })
    .then(notFound(res))
    .then((chatMessages) => chatMessages.map((chatMessage) => chatMessage.view()))
    .then(success(res))
    .catch(next)
    
export const update = ({ bodymen: { body }, params }, res, next) =>
  ChatMessage.findById(params.id)
    .then(notFound(res))
    .then((chatMessage) => chatMessage ? Object.assign(chatMessage, body).save() : null)
    .then((chatMessage) => chatMessage ? chatMessage.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  ChatMessage.findById(params.id)
    .then(notFound(res))
    .then((chatMessage) => chatMessage ? chatMessage.remove() : null)
    .then(success(res, 204))
    .catch(next)
