import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, showMessageByRoomId } from './controller'
import { schema } from './model'
export ChatMessage, { schema } from './model'

const router = new Router()
const { message, isRead, roomId, from, isBot } = schema.tree

/**
 * @api {post} /chatMessages Create chat message
 * @apiName CreateChatMessage
 * @apiGroup ChatMessage
 * @apiParam message Chat message's message.
 * @apiParam isRead Chat message's isRead.
 * @apiParam roomId Chat message's roomId.
 * @apiParam from Chat message's from.
 * @apiParam isBot Chat message's isBot.
 * @apiSuccess {Object} chatMessage Chat message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat message not found.
 */
router.post('/',
  body({ message, isRead, roomId, from, isBot }),
  create)

/**
 * @api {get} /chatMessages Retrieve chat messages
 * @apiName RetrieveChatMessages
 * @apiGroup ChatMessage
 * @apiUse listParams
 * @apiSuccess {Object[]} chatMessages List of chat messages.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /chatMessages/:id Retrieve chat message
 * @apiName RetrieveChatMessage
 * @apiGroup ChatMessage
 * @apiSuccess {Object} chatMessage Chat message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat message not found.
 */
router.get('/:id',
  show)

/**
* @api {get} /chatMessages/roomId/:roomId Retrieve chat message
* @apiName RetrieveChatMessage
* @apiGroup ChatMessage
* @apiSuccess {Object} chatMessage Chat message's data.
* @apiError {Object} 400 Some parameters may contain invalid values.
* @apiError 404 Chat message not found.
*/
router.get('/roomId/:roomId',
  showMessageByRoomId)
  
/**
 * @api {put} /chatMessages/:id Update chat message
 * @apiName UpdateChatMessage
 * @apiGroup ChatMessage
 * @apiParam message Chat message's message.
 * @apiParam isRead Chat message's isRead.
 * @apiParam roomId Chat message's roomId.
 * @apiParam from Chat message's from.
 * @apiParam isBot Chat message's isBot.
 * @apiSuccess {Object} chatMessage Chat message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat message not found.
 */
router.put('/:id',
  body({ message, isRead, roomId, from, isBot }),
  update)

/**
 * @api {delete} /chatMessages/:id Delete chat message
 * @apiName DeleteChatMessage
 * @apiGroup ChatMessage
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Chat message not found.
 */
router.delete('/:id',
  destroy)

export default router
