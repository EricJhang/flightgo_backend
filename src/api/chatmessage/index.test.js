import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { ChatMessage } from '.'

const app = () => express(apiRoot, routes)

let chatMessage

beforeEach(async () => {
  chatMessage = await ChatMessage.create({})
})

test('POST /chatMessages 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ message: 'test', isRead: 'test', roomId: 'test', from: 'test', isBot: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.message).toEqual('test')
  expect(body.isRead).toEqual('test')
  expect(body.roomId).toEqual('test')
  expect(body.from).toEqual('test')
  expect(body.isBot).toEqual('test')
})

test('GET /chatMessages 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /chatMessages/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${chatMessage.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(chatMessage.id)
})

test('GET /chatMessages/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /chatMessages/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${chatMessage.id}`)
    .send({ message: 'test', isRead: 'test', roomId: 'test', from: 'test', isBot: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(chatMessage.id)
  expect(body.message).toEqual('test')
  expect(body.isRead).toEqual('test')
  expect(body.roomId).toEqual('test')
  expect(body.from).toEqual('test')
  expect(body.isBot).toEqual('test')
})

test('PUT /chatMessages/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ message: 'test', isRead: 'test', roomId: 'test', from: 'test', isBot: 'test' })
  expect(status).toBe(404)
})

test('DELETE /chatMessages/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${chatMessage.id}`)
  expect(status).toBe(204)
})

test('DELETE /chatMessages/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
