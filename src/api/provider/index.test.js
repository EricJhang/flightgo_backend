import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Provider } from '.'

const app = () => express(apiRoot, routes)

let provider

beforeEach(async () => {
  provider = await Provider.create({})
})

test('POST /providers 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ providerId: 'test', providerName: 'test', businessPlan: 'test', providerType: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.providerId).toEqual('test')
  expect(body.providerName).toEqual('test')
  expect(body.businessPlan).toEqual('test')
  expect(body.providerType).toEqual('test')
})

test('GET /providers 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /providers/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${provider.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(provider.id)
})

test('GET /providers/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /providers/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${provider.id}`)
    .send({ providerId: 'test', providerName: 'test', businessPlan: 'test', providerType: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(provider.id)
  expect(body.providerId).toEqual('test')
  expect(body.providerName).toEqual('test')
  expect(body.businessPlan).toEqual('test')
  expect(body.providerType).toEqual('test')
})

test('PUT /providers/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ providerId: 'test', providerName: 'test', businessPlan: 'test', providerType: 'test' })
  expect(status).toBe(404)
})

test('DELETE /providers/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${provider.id}`)
  expect(status).toBe(204)
})

test('DELETE /providers/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
