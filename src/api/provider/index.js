import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Provider, { schema } from './model'

const router = new Router()
const { providerId, providerName, businessPlan, providerType } = schema.tree

/**
 * @api {post} /providers Create provider (a travel agent)
 * @apiName CreateProvider
 * @apiGroup Provider
 * @apiParam providerId Provider's providerId.
 * @apiParam providerName Provider's providerName.
 * @apiParam businessPlan Provider's businessPlan.
 * @apiParam providerType Provider's providerType.
 * @apiSuccess {Object} provider Provider's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Provider not found.
 */
router.post('/',
  body({ providerId, providerName, businessPlan, providerType }),
  create)

/**
 * @api {get} /providers Retrieve providers
 * @apiName RetrieveProviders
 * @apiGroup Provider
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of providers.
 * @apiSuccess {Object[]} rows List of providers.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /providers/:id Retrieve provider
 * @apiName RetrieveProvider
 * @apiGroup Provider
 * @apiSuccess {Object} provider Provider's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Provider not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /providers/:id Update provider
 * @apiName UpdateProvider
 * @apiGroup Provider
 * @apiParam providerId Provider's providerId.
 * @apiParam providerName Provider's providerName.
 * @apiParam businessPlan Provider's businessPlan.
 * @apiParam providerType Provider's providerType.
 * @apiSuccess {Object} provider Provider's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Provider not found.
 */
router.put('/:id',
  body({ providerId, providerName, businessPlan, providerType }),
  update)

/**
 * @api {delete} /providers/:id Delete provider
 * @apiName DeleteProvider
 * @apiGroup Provider
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Provider not found.
 */
router.delete('/:id',
  destroy)

export default router
