import { Provider } from '.'

let provider

beforeEach(async () => {
  provider = await Provider.create({ providerId: 'test', providerName: 'test', businessPlan: 'test', providerType: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = provider.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(provider.id)
    expect(view.providerId).toBe(provider.providerId)
    expect(view.providerName).toBe(provider.providerName)
    expect(view.businessPlan).toBe(provider.businessPlan)
    expect(view.providerType).toBe(provider.providerType)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = provider.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(provider.id)
    expect(view.providerId).toBe(provider.providerId)
    expect(view.providerName).toBe(provider.providerName)
    expect(view.businessPlan).toBe(provider.businessPlan)
    expect(view.providerType).toBe(provider.providerType)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
