import mongoose, { Schema } from 'mongoose'

const providerSchema = new Schema({
  providerId: {
    type: String
  },
  providerName: {
    type: String
  },
  businessPlan: {
    type: String
  },
  providerType: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

providerSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      providerId: this.providerId,
      providerName: this.providerName,
      businessPlan: this.businessPlan,
      providerType: this.providerType,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Provider', providerSchema)

export const schema = model.schema
export default model
