import { Router } from 'express'
import user from './user'
import auth from './auth'
import tag from './tag'
import pushMessage from './pushmessage'
import lineUser from './lineuser'
import chatRoom from './chatroom'
import chatMessage from './chatmessage'
import provider from './provider'

//
//a
const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/tags', tag)
router.use('/push', pushMessage) // PUSH MESSAGE to LINE  
router.use('/chatRooms', chatRoom) // CHAT ROOM API
router.use('/chatMessages', chatMessage) // CHAT MESSAGE API
router.use('/lineUsers', lineUser)// LINE USERS API
router.use('/providers', provider)


export default router
