import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy  } from './controller'
import { schema } from './model'
export LineUser, { schema } from './model'

const router = new Router()
const { userId, name, email, gender, phoneNumber, pictureUrl, favorite, age, providerId, chatRoomId } = schema.tree

/**
 * @api {post} /lineUsers Create line user
 * @apiName CreateLineUser
 * @apiGroup LineUser
 * @apiParam userId Line user's userId.
 * @apiParam name Line user's name.
 * @apiParam email Line user's email.
 * @apiParam gender Line user's gender.
 * @apiParam phoneNumber Line user's phoneNumber.
 * @apiParam pictureUrl Line user's pictureUrl.
 * @apiParam favorite Line user's favorite.
 * @apiParam age Line user's age.
 * @apiParam providerId Line user's providerId.
 * @apiParam chatRoomId Line user's chatRoomId.
 * @apiSuccess {Object} lineUser Line user's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Line user not found.
 */
router.post('/',
  body({ userId, name, email, gender, phoneNumber, pictureUrl, favorite, age, providerId, chatRoomId }),
  create)

/**
 * @api {get} /lineUsers Retrieve line users
 * @apiName RetrieveLineUsers
 * @apiGroup LineUser
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of line users.
 * @apiSuccess {Object[]} rows List of line users.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /lineUsers/:id Retrieve line user
 * @apiName RetrieveLineUser
 * @apiGroup LineUser
 * @apiSuccess {Object} lineUser Line user's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Line user not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /lineUsers/:id Update line user
 * @apiName UpdateLineUser
 * @apiGroup LineUser
 * @apiParam userId Line user's userId.
 * @apiParam name Line user's name.
 * @apiParam email Line user's email.
 * @apiParam gender Line user's gender.
 * @apiParam phoneNumber Line user's phoneNumber.
 * @apiParam pictureUrl Line user's pictureUrl.
 * @apiParam favorite Line user's favorite.
 * @apiParam age Line user's age.
 * @apiParam providerId Line user's providerId.
 * @apiParam chatRoomId Line user's chatRoomId.
 * @apiSuccess {Object} lineUser Line user's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Line user not found.
 */
router.put('/:id',
  body({ userId, name, email, gender, phoneNumber, pictureUrl, favorite, age, providerId, chatRoomId }),
  update)

/**
 * @api {delete} /lineUsers/:id Delete line user
 * @apiName DeleteLineUser
 * @apiGroup LineUser
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Line user not found.
 */
router.delete('/:id',
  destroy)

export default router
