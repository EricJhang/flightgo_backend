import { success, notFound } from '../../services/response/'
import { LineUser } from '.'

export const create = function ({ bodymen: { body } }, res, next) {
  // add catRoomId
  body.chatRoomId = body.providerId + "_" + body.userId
  console.log(body.chatRoomId)
  LineUser.create(body)
    .then((lineUser) => lineUser.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  LineUser.count(query)
    .then(count => LineUser.find(query, select, cursor)
      .then((lineUsers) => ({
        count,
        rows: lineUsers.map((lineUser) => lineUser.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  LineUser.findById(params.id)
    .then(notFound(res))
    .then((lineUser) => lineUser ? lineUser.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  LineUser.findById(params.id)
    .then(notFound(res))
    .then((lineUser) => lineUser ? Object.assign(lineUser, body).save() : null)
    .then((lineUser) => lineUser ? lineUser.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  LineUser.findById(params.id)
    .then(notFound(res))
    .then((lineUser) => lineUser ? lineUser.remove() : null)
    .then(success(res, 204))
    .catch(next)
