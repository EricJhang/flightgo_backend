import { LineUser } from '.'

let lineUser

beforeEach(async () => {
  lineUser = await LineUser.create({ userId: 'test', name: 'test', email: 'test', gender: 'test', phoneNumber: 'test', pictureUrl: 'test', favorite: 'test', age: 'test', providerId: 'test', chatRoomId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = lineUser.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(lineUser.id)
    expect(view.userId).toBe(lineUser.userId)
    expect(view.name).toBe(lineUser.name)
    expect(view.email).toBe(lineUser.email)
    expect(view.gender).toBe(lineUser.gender)
    expect(view.phoneNumber).toBe(lineUser.phoneNumber)
    expect(view.pictureUrl).toBe(lineUser.pictureUrl)
    expect(view.favorite).toBe(lineUser.favorite)
    expect(view.age).toBe(lineUser.age)
    expect(view.providerId).toBe(lineUser.providerId)
    expect(view.chatRoomId).toBe(lineUser.chatRoomId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = lineUser.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(lineUser.id)
    expect(view.userId).toBe(lineUser.userId)
    expect(view.name).toBe(lineUser.name)
    expect(view.email).toBe(lineUser.email)
    expect(view.gender).toBe(lineUser.gender)
    expect(view.phoneNumber).toBe(lineUser.phoneNumber)
    expect(view.pictureUrl).toBe(lineUser.pictureUrl)
    expect(view.favorite).toBe(lineUser.favorite)
    expect(view.age).toBe(lineUser.age)
    expect(view.providerId).toBe(lineUser.providerId)
    expect(view.chatRoomId).toBe(lineUser.chatRoomId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
