import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { LineUser } from '.'

const app = () => express(apiRoot, routes)

let lineUser

beforeEach(async () => {
  lineUser = await LineUser.create({})
})

test('POST /lineUsers 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ userId: 'test', name: 'test', email: 'test', gender: 'test', phoneNumber: 'test', pictureUrl: 'test', favorite: 'test', age: 'test', providerId: 'test', chatRoomId: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.userId).toEqual('test')
  expect(body.name).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.gender).toEqual('test')
  expect(body.phoneNumber).toEqual('test')
  expect(body.pictureUrl).toEqual('test')
  expect(body.favorite).toEqual('test')
  expect(body.age).toEqual('test')
  expect(body.providerId).toEqual('test')
  expect(body.chatRoomId).toEqual('test')
})

test('GET /lineUsers 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /lineUsers/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${lineUser.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(lineUser.id)
})

test('GET /lineUsers/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /lineUsers/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${lineUser.id}`)
    .send({ userId: 'test', name: 'test', email: 'test', gender: 'test', phoneNumber: 'test', pictureUrl: 'test', favorite: 'test', age: 'test', providerId: 'test', chatRoomId: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(lineUser.id)
  expect(body.userId).toEqual('test')
  expect(body.name).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.gender).toEqual('test')
  expect(body.phoneNumber).toEqual('test')
  expect(body.pictureUrl).toEqual('test')
  expect(body.favorite).toEqual('test')
  expect(body.age).toEqual('test')
  expect(body.providerId).toEqual('test')
  expect(body.chatRoomId).toEqual('test')
})

test('PUT /lineUsers/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ userId: 'test', name: 'test', email: 'test', gender: 'test', phoneNumber: 'test', pictureUrl: 'test', favorite: 'test', age: 'test', providerId: 'test', chatRoomId: 'test' })
  expect(status).toBe(404)
})

test('DELETE /lineUsers/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${lineUser.id}`)
  expect(status).toBe(204)
})

test('DELETE /lineUsers/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
