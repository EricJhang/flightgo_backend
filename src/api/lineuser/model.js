import mongoose, { Schema } from 'mongoose'

const lineUserSchema = new Schema({
  userId: {
    type: String
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  gender: {
    type: Boolean
  },
  phoneNumber: {
    type: Number
  },
  pictureUrl: {
    type: String
  },
  favorite: {
    type: String
  },
  age: {
    type: Number
  },
  providerId: {
    type: String
  },
  chatRoomId: {
    type: String
  }
}, {
    timestamps: true,
    toJSON: {
      virtuals: true,
      transform: (obj, ret) => { delete ret._id }
    }
  })

lineUserSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      userId: this.userId,
      name: this.name,
      email: this.email,
      gender: this.gender,
      phoneNumber: this.phoneNumber,
      pictureUrl: this.pictureUrl,
      favorite: this.favorite,
      age: this.age,
      providerId: this.providerId,
      chatRoomId: this.providerId + "_" + this.userId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('lineuser', lineUserSchema)

export const schema = model.schema
export default model
