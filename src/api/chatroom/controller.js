import { success, notFound } from '../../services/response/'
import { ChatRoom } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  ChatRoom.create(body)
    .then((chatRoom) => chatRoom.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  ChatRoom.find(query, select, cursor)
    .then((chatRooms) => chatRooms.map((chatRoom) => chatRoom.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  ChatRoom.findById(params.id)
    .then(notFound(res))
    .then((chatRoom) => chatRoom ? chatRoom.view() : null)
    .then(success(res))
    .catch(next)

export const showRoom = ({ params }, res, next) =>
  ChatRoom.find({ roomId: params.roomId })
    .then(notFound(res))
    .then((chatRooms) => chatRooms.map((chatRoom) => chatRoom.view()))
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  ChatRoom.findById(params.id)
    .then(notFound(res))
    .then((chatRoom) => chatRoom ? Object.assign(chatRoom, body).save() : null)
    .then((chatRoom) => chatRoom ? chatRoom.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  ChatRoom.findById(params.id)
    .then(notFound(res))
    .then((chatRoom) => chatRoom ? chatRoom.remove() : null)
    .then(success(res, 204))
    .catch(next)
