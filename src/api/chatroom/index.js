import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy ,showRoom} from './controller'
import { schema } from './model'
export ChatRoom, { schema } from './model'

const router = new Router()
const { userId, providerId, roomId } = schema.tree

/**
 * @api {post} /chatRooms Create chat room
 * @apiName CreateChatRoom
 * @apiGroup ChatRoom
 * @apiParam userId Chat room's userId.
 * @apiParam providerId Chat room's providerId.
 * @apiParam roomId Chat room's roomId.
 * @apiSuccess {Object} chatRoom Chat room's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat room not found.
 */
router.post('/',
  body({ userId, providerId, roomId }),
  create)

/**
 * @api {get} /chatRooms Retrieve chat rooms
 * @apiName RetrieveChatRooms
 * @apiGroup ChatRoom
 * @apiUse listParams
 * @apiSuccess {Object[]} chatRooms List of chat rooms.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /chatRooms/:id Retrieve chat room
 * @apiName RetrieveChatRoom
 * @apiGroup ChatRoom
 * @apiSuccess {Object} chatRoom Chat room's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat room not found.
 */
router.get('/:id',
  show)

/**
 * @api {get} /chatRooms/roomId/:roomId Retrieve chat room
 * @apiName RetrieveChatRoom
 * @apiGroup ChatRoom
 * @apiSuccess {Object} chatRoom Chat room's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat room not found.
 */
router.get('/roomId/:roomId',
  showRoom)

/**
 * @api {put} /chatRooms/:id Update chat room
 * @apiName UpdateChatRoom
 * @apiGroup ChatRoom
 * @apiParam userId Chat room's userId.
 * @apiParam providerId Chat room's providerId.
 * @apiParam roomId Chat room's roomId.
 * @apiSuccess {Object} chatRoom Chat room's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Chat room not found.
 */
router.put('/:id',
  body({ userId, providerId, roomId }),
  update)

/**
 * @api {delete} /chatRooms/:id Delete chat room
 * @apiName DeleteChatRoom
 * @apiGroup ChatRoom
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Chat room not found.
 */
router.delete('/:id',
  destroy)

export default router
