import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { ChatRoom } from '.'

const app = () => express(apiRoot, routes)

let chatRoom

beforeEach(async () => {
  chatRoom = await ChatRoom.create({})
})

test('POST /chatRooms 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ userId: 'test', providerId: 'test', roomId: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.userId).toEqual('test')
  expect(body.providerId).toEqual('test')
  expect(body.roomId).toEqual('test')
})

test('GET /chatRooms 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /chatRooms/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${chatRoom.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(chatRoom.id)
})

test('GET /chatRooms/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /chatRooms/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${chatRoom.id}`)
    .send({ userId: 'test', providerId: 'test', roomId: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(chatRoom.id)
  expect(body.userId).toEqual('test')
  expect(body.providerId).toEqual('test')
  expect(body.roomId).toEqual('test')
})

test('PUT /chatRooms/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ userId: 'test', providerId: 'test', roomId: 'test' })
  expect(status).toBe(404)
})

test('DELETE /chatRooms/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${chatRoom.id}`)
  expect(status).toBe(204)
})

test('DELETE /chatRooms/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
