import { ChatRoom } from '.'

let chatRoom

beforeEach(async () => {
  chatRoom = await ChatRoom.create({ userId: 'test', providerId: 'test', roomId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = chatRoom.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(chatRoom.id)
    expect(view.userId).toBe(chatRoom.userId)
    expect(view.providerId).toBe(chatRoom.providerId)
    expect(view.roomId).toBe(chatRoom.roomId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = chatRoom.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(chatRoom.id)
    expect(view.userId).toBe(chatRoom.userId)
    expect(view.providerId).toBe(chatRoom.providerId)
    expect(view.roomId).toBe(chatRoom.roomId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
