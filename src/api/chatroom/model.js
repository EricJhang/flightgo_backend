import mongoose, { Schema } from 'mongoose'

const chatRoomSchema = new Schema({
  userId: {
    type: String
  },
  providerId: {
    type: String
  },
  roomId: {
    type: String
  }
}, {
    timestamps: true,
    toJSON: {
      virtuals: true,
      transform: (obj, ret) => { delete ret._id }
    }
  })

chatRoomSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      userId: this.userId,
      providerId: this.providerId,
      roomId: this.roomId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('chatroom', chatRoomSchema)

export const schema = model.schema
export default model
