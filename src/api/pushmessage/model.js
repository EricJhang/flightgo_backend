import mongoose, { Schema } from 'mongoose'

const pushMessageSchema = new Schema({
  users: {
    type: String
  },
  message: {
    type: String
  },
  tags: {
    type: String
  },
  providerId: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

pushMessageSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      users: this.users,
      message: this.message,
      tags: this.tags,
      providerId: this.providerId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}
//
const model = mongoose.model('pushmessage', pushMessageSchema)

export const schema = model.schema
export default model
