import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { PushMessage } from '.'

const app = () => express(apiRoot, routes)

let pushMessage

beforeEach(async () => {
  pushMessage = await PushMessage.create({})
})

test('POST /push 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ users: 'test', message: 'test', tags: 'test', providerId: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.users).toEqual('test')
  expect(body.message).toEqual('test')
  expect(body.tags).toEqual('test')
  expect(body.providerId).toEqual('test')
})

test('GET /push 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /push/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${pushMessage.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(pushMessage.id)
})

test('GET /push/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /push/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${pushMessage.id}`)
    .send({ users: 'test', message: 'test', tags: 'test', providerId: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(pushMessage.id)
  expect(body.users).toEqual('test')
  expect(body.message).toEqual('test')
  expect(body.tags).toEqual('test')
  expect(body.providerId).toEqual('test')
})

test('PUT /push/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ users: 'test', message: 'test', tags: 'test', providerId: 'test' })
  expect(status).toBe(404)
})

test('DELETE /push/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${pushMessage.id}`)
  expect(status).toBe(204)
})

test('DELETE /push/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
