import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export PushMessage, { schema } from './model'

const router = new Router()
const { users, message, tags, providerId } = schema.tree

/**
 * @api {post} /push Create push message
 * @apiName CreatePushMessage
 * @apiGroup PushMessage
 * @apiParam users Push message's users. User ID:U3c85a6aee17d4e707b2d88e46e41aad8
 * @apiParam message Push message's message.
 * @apiParam tags Push message's tags.
 * @apiParam providerId Push message's providerId.
 * @apiSuccess {Object} pushMessage Push message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Push message not found.
 */
router.post('/',
  body({ users, message, tags, providerId }),
  create)

/**
 * @api {get} /push Retrieve push messages
 * @apiName RetrievePushMessages
 * @apiGroup PushMessage
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of push messages.
 * @apiSuccess {Object[]} rows List of push messages.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /push/:id Retrieve push message
 * @apiName RetrievePushMessage
 * @apiGroup PushMessage
 * @apiSuccess {Object} pushMessage Push message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Push message not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /push/:id Update push message
 * @apiName UpdatePushMessage
 * @apiGroup PushMessage
 * @apiParam users Push message's users.
 * @apiParam message Push message's message.
 * @apiParam tags Push message's tags.
 * @apiParam providerId Push message's providerId.
 * @apiSuccess {Object} pushMessage Push message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Push message not found.
 */
router.put('/:id',
  body({ users, message, tags, providerId }),
  update)

/**
 * @api {delete} /push/:id Delete push message
 * @apiName DeletePushMessage
 * @apiGroup PushMessage
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Push message not found.
 */
router.delete('/:id',
  destroy)

export default router
