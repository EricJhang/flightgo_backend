import { PushMessage } from '.'

let pushMessage

beforeEach(async () => {
  pushMessage = await PushMessage.create({ users: 'test', message: 'test', tags: 'test', providerId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = pushMessage.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(pushMessage.id)
    expect(view.users).toBe(pushMessage.users)
    expect(view.message).toBe(pushMessage.message)
    expect(view.tags).toBe(pushMessage.tags)
    expect(view.providerId).toBe(pushMessage.providerId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = pushMessage.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(pushMessage.id)
    expect(view.users).toBe(pushMessage.users)
    expect(view.message).toBe(pushMessage.message)
    expect(view.tags).toBe(pushMessage.tags)
    expect(view.providerId).toBe(pushMessage.providerId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
