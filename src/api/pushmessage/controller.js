import { success, notFound } from '../../services/response/'
import { PushMessage } from '.'
const line = require('@line/bot-sdk');
// const config = {
//   channelAccessToken: '',
//   channelSecret: '3e4adf55ff56402454852e729b026244'
// };
// const client = new line.Client(config);
// line.middleware(config);

export const create = ({ bodymen: { body } }, res, next) =>
  PushMessage.create(body)
    .then(pushMessage => {
      pushMessage.view(true)
      let _userId = body.users;
      let _message = body.message;
      
      const client = new line.Client({
        channelAccessToken: 'O3kDD5t80yC4I8bCWHUsp0DRRm2ZVl/Y5n2mBKczD4bIxbedCF8CA2B6fSNu0/12jO5k0wbp8etkBa6HVwb6T2a5jCQOOMKovAqq7dbJ0hTUoxfez+u5scFr5xWrH2hjxmu9ZcjKmrK7x9alXFSiTgdB04t89/1O/w1cDnyilFU='
      });
      const message = {
        type: 'text',
        text: _message
      }
      // userId = 'U3c85a6aee17d4e707b2d88e46e41aad8'
      client.pushMessage(_userId, message)
        .then(success(res, 201))
    })
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  PushMessage.count(query)
    .then(count => PushMessage.find(query, select, cursor)
      .then((pushMessages) => ({
        count,
        rows: pushMessages.map((pushMessage) => pushMessage.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  PushMessage.findById(params.id)
    .then(notFound(res))
    .then((pushMessage) => pushMessage ? pushMessage.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  PushMessage.findById(params.id)
    .then(notFound(res))
    .then((pushMessage) => pushMessage ? Object.assign(pushMessage, body).save() : null)
    .then((pushMessage) => pushMessage ? pushMessage.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  PushMessage.findById(params.id)
    .then(notFound(res))
    .then((pushMessage) => pushMessage ? pushMessage.remove() : null)
    .then(success(res, 204))
    .catch(next)
