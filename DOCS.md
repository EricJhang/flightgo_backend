# flightgo-backend v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [ChatMessage](#chatmessage)
	- [Create chat message](#create-chat-message)
	- [Delete chat message](#delete-chat-message)
	- [Retrieve chat message](#retrieve-chat-message)
	- [Retrieve chat messages](#retrieve-chat-messages)
	- [Update chat message](#update-chat-message)
	
- [ChatRoom](#chatroom)
	- [Create chat room](#create-chat-room)
	- [Delete chat room](#delete-chat-room)
	- [Retrieve chat room](#retrieve-chat-room)
	- [Retrieve chat rooms](#retrieve-chat-rooms)
	- [Update chat room](#update-chat-room)
	
- [LineUser](#lineuser)
	- [Create line user](#create-line-user)
	- [Delete line user](#delete-line-user)
	- [Retrieve line user](#retrieve-line-user)
	- [Retrieve line users](#retrieve-line-users)
	- [Update line user](#update-line-user)
	
- [Provider](#provider)
	- [Create provider](#create-provider)
	- [Delete provider](#delete-provider)
	- [Retrieve provider](#retrieve-provider)
	- [Retrieve providers](#retrieve-providers)
	- [Update provider](#update-provider)
	
- [PushMessage](#pushmessage)
	- [Create push message](#create-push-message)
	- [Delete push message](#delete-push-message)
	- [Retrieve push message](#retrieve-push-message)
	- [Retrieve push messages](#retrieve-push-messages)
	- [Update push message](#update-push-message)
	
- [Tag](#tag)
	- [Create tag](#create-tag)
	- [Delete tag](#delete-tag)
	- [Retrieve tag](#retrieve-tag)
	- [Retrieve tags](#retrieve-tags)
	- [Update tag](#update-tag)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# ChatMessage

## Create chat message



	POST /chatMessages


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| message			| 			|  <p>Chat message's message.</p>							|
| isRead			| 			|  <p>Chat message's isRead.</p>							|
| roomId			| 			|  <p>Chat message's roomId.</p>							|
| from			| 			|  <p>Chat message's from.</p>							|
| isBot			| 			|  <p>Chat message's isBot.</p>							|

## Delete chat message



	DELETE /chatMessages/:id


## Retrieve chat message



	GET /chatMessages/:id


## Retrieve chat messages



	GET /chatMessages


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update chat message



	PUT /chatMessages/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| message			| 			|  <p>Chat message's message.</p>							|
| isRead			| 			|  <p>Chat message's isRead.</p>							|
| roomId			| 			|  <p>Chat message's roomId.</p>							|
| from			| 			|  <p>Chat message's from.</p>							|
| isBot			| 			|  <p>Chat message's isBot.</p>							|

# ChatRoom

## Create chat room



	POST /chatRooms


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Chat room's userId.</p>							|
| providerId			| 			|  <p>Chat room's providerId.</p>							|
| roomId			| 			|  <p>Chat room's roomId.</p>							|

## Delete chat room



	DELETE /chatRooms/:id


## Retrieve chat room



	GET /chatRooms/roomId/:roomId


## Retrieve chat rooms



	GET /chatRooms


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update chat room



	PUT /chatRooms/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Chat room's userId.</p>							|
| providerId			| 			|  <p>Chat room's providerId.</p>							|
| roomId			| 			|  <p>Chat room's roomId.</p>							|

# LineUser

## Create line user



	POST /lineUsers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Line user's userId.</p>							|
| name			| 			|  <p>Line user's name.</p>							|
| email			| 			|  <p>Line user's email.</p>							|
| gender			| 			|  <p>Line user's gender.</p>							|
| phoneNumber			| 			|  <p>Line user's phoneNumber.</p>							|
| pictureUrl			| 			|  <p>Line user's pictureUrl.</p>							|
| favorite			| 			|  <p>Line user's favorite.</p>							|
| age			| 			|  <p>Line user's age.</p>							|
| providerId			| 			|  <p>Line user's providerId.</p>							|
| chatRoomId			| 			|  <p>Line user's chatRoomId.</p>							|

## Delete line user



	DELETE /lineUsers/:id


## Retrieve line user



	GET /lineUsers/:id


## Retrieve line users



	GET /lineUsers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update line user



	PUT /lineUsers/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| userId			| 			|  <p>Line user's userId.</p>							|
| name			| 			|  <p>Line user's name.</p>							|
| email			| 			|  <p>Line user's email.</p>							|
| gender			| 			|  <p>Line user's gender.</p>							|
| phoneNumber			| 			|  <p>Line user's phoneNumber.</p>							|
| pictureUrl			| 			|  <p>Line user's pictureUrl.</p>							|
| favorite			| 			|  <p>Line user's favorite.</p>							|
| age			| 			|  <p>Line user's age.</p>							|
| providerId			| 			|  <p>Line user's providerId.</p>							|
| chatRoomId			| 			|  <p>Line user's chatRoomId.</p>							|

# Provider

## Create provider



	POST /providers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| providerId			| 			|  <p>Provider's providerId.</p>							|
| providerName			| 			|  <p>Provider's providerName.</p>							|
| businessPlan			| 			|  <p>Provider's businessPlan.</p>							|
| providerType			| 			|  <p>Provider's providerType.</p>							|

## Delete provider



	DELETE /providers/:id


## Retrieve provider



	GET /providers/:id


## Retrieve providers



	GET /providers


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update provider



	PUT /providers/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| providerId			| 			|  <p>Provider's providerId.</p>							|
| providerName			| 			|  <p>Provider's providerName.</p>							|
| businessPlan			| 			|  <p>Provider's businessPlan.</p>							|
| providerType			| 			|  <p>Provider's providerType.</p>							|

# PushMessage

## Create push message



	POST /push


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| users			| 			|  <p>Push message's users. User ID:U3c85a6aee17d4e707b2d88e46e41aad8</p>							|
| message			| 			|  <p>Push message's message.</p>							|
| tags			| 			|  <p>Push message's tags.</p>							|
| providerId			| 			|  <p>Push message's providerId.</p>							|

## Delete push message



	DELETE /push/:id


## Retrieve push message



	GET /push/:id


## Retrieve push messages



	GET /push


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update push message



	PUT /push/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| users			| 			|  <p>Push message's users.</p>							|
| message			| 			|  <p>Push message's message.</p>							|
| tags			| 			|  <p>Push message's tags.</p>							|
| providerId			| 			|  <p>Push message's providerId.</p>							|

# Tag

## Create tag



	POST /tags


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| title			| 			|  <p>Tag's title.</p>							|

## Delete tag



	DELETE /tags/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>admin access token.</p>							|

## Retrieve tag



	GET /tags/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|

## Retrieve tags



	GET /tags


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update tag



	PUT /tags/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>user access token.</p>							|
| title			| 			|  <p>Tag's title.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


